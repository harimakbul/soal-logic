﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoalLogic
{
    class BankTarmin
    {
        public static void GetSaldo() { 
            int tahun_masuk, tahun_keluar, saldo, jml_potongan;


            Console.Write("Tahun Awal Menabung: ");
            tahun_masuk = int.Parse(Console.ReadLine());

            Console.Write("Masukkan jumlah uang: ");
            saldo = int.Parse(Console.ReadLine());

            Console.Write("Tahun Akhir Menabung: ");
            tahun_keluar = int.Parse(Console.ReadLine());


            for(int i = tahun_masuk; i <= tahun_keluar; i++)
            {
                jml_potongan = saldo * 5 / 100;
                saldo = saldo - jml_potongan;
            }

            Console.WriteLine("Total saldo di tahun " + tahun_keluar + " = " + saldo);
        }
    }
}
