﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoalLogic
{
    class DendaBuku
    {
        public static void GetDenda()
        {
            int jml_hari_buku_pelajaran, jml_hari_buku_novel, jml_hari_skripsi, total_denda;
            Console.Write("Buku Pelajaran | Jumlah Hari: ");
            jml_hari_buku_pelajaran = int.Parse(Console.ReadLine());

            Console.Write("Buku Novel | Jumlah Hari: ");
            jml_hari_buku_novel = int.Parse(Console.ReadLine());

            Console.Write("Buku Skripsi | Jumlah Hari: ");
            jml_hari_skripsi = int.Parse(Console.ReadLine());

            int total_denda_pelajaran = jml_hari_buku_pelajaran > 10 ? (jml_hari_buku_pelajaran - 10) * 2000 : 0;
            int total_denda_novel = jml_hari_buku_novel > 10 ? (jml_hari_buku_novel - 10) * 5000 : 0; 
            int total_denda_skripsi = jml_hari_skripsi > 10 ? (jml_hari_skripsi - 10) * 10000 : 0;

            total_denda = total_denda_pelajaran + total_denda_novel + total_denda_skripsi;

            Console.WriteLine("Total Denda adalah " + total_denda);
        }
    }
}
