﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoalLogic
{
    class Kelipatan
    {
        public static void GetResultData()
        {
            int number;
            Console.Write("Masukkan Nilai: ");
            number = int.Parse(Console.ReadLine());

            if(number % 5 == 0)
            {
                if(number <= 60)
                {
                    Console.WriteLine("Kurang");
                } else if(number > 60 && number <= 70)
                {
                    Console.WriteLine("Cukup");
                } else if(number > 70 && number <= 80)
                {
                    Console.WriteLine("Baik");
                } else if(number > 80)
                {
                    Console.WriteLine("Luar Biasa");
                } else if(number > 100)
                {
                    Console.WriteLine("nilai luar biasa");
                }
            } else
            {
                Console.WriteLine("Bukan Bilangan Kelipatan 5");
            }
        }
    }
}
