﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoalLogic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bilangan Prima");
            BilanganPrima.GetBilanganPrima();

            Console.WriteLine("Bilangan Fibonacci");
            Fibonacci.GetAllFibonacci();

            Console.WriteLine("BANK TARMIN");
            BankTarmin.GetSaldo();

            Console.WriteLine("Bilangan Kelipatan");
            Kelipatan.GetResultData();

            Console.WriteLine("Denda Buku");
            DendaBuku.GetDenda();

            Console.ReadKey();
        }
    }
}
